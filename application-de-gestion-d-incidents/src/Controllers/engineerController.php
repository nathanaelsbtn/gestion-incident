<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class engineerController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/engineer/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        // $bugs = [];
        $manager = new BugManager();

        $bugs = $manager->findAll();
        // TODO: liste des incidents
        
        $content = $this->render('src/Views/Bug/engineer/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function update(Bug $bugs){

        // Update d'un incident en BDD
       $dbh = static::connectDb();

       $req = $dbh->prepare('UPDATE bug
                             SET closed = :clotureDate
                             WHERE id = '.$bugs->getId()
                           );

       $req->execute(array(

              'clotureDate' => date("Y-m-d H:i:s")

       ));
    }

    public function logout(){

        session_destroy();
        unset($_SESSION);
        header('Location: '. PUBLIC_PATH .'login');
    }

}
