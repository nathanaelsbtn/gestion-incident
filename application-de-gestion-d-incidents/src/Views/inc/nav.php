<nav class="blue-grey lighten-2" role="navigation">

    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">GESTION INCIDENT</a>
       <ul class="right hide-on-med-and-down">
     
        <?php
            if(isset($_SESSION['user'])){
                $user = $_SESSION['user'];
                
              ?>

              
            <li>
                <a href="#" class="user">
                    <span class="username"></span>
                </a>

            </li>

            <li>
              <a href="logout" class="user">
                  Logout  <?=$user->getNom();?>
              </a>
            </li>
        <?php
        }
        ?>
    </ul>

    <ul id="nav-mobile" class="sidenav">
      <li><a href="#"></a></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav> 
 