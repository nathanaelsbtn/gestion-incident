<?php

if(isset( $parameters['error'])){
    $error = $parameters['error'];
}

include (__DIR__.'./../inc/header.php');
include (__DIR__.'./../inc/footer.php');


?>

<nav class="blue-grey lighten-2" role="navigation">
<div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">GESTION INCIDENT</a>
    
      <ul class="right hide-on-med-and-down">
      </ul>
      <ul id="nav-mobile" class="sidenav">
      </ul>

    </div>
  </nav>

    <body>

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-grey-text text-darken-4">Login</h1>
        </div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row"><?php if(isset($error)) echo $error;?></div>

            <div class="row">
                <form class="col s12" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="email" id="email" type="email" class="validate" name="email">
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field col s6">
                            <input placeholder="password"  id="password" type="password" class="validate" name="password">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <button class="btn right" type="submit" name="submit">Login

                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

