<?php

    /** @var $bug \BugApp\Models\Bug */

    $bugs = $parameters['bugs'];
    include ('../src/Views/inc/header.php');
    include ('../src/Views/inc/nav.php');

?>

<!DOCTYPE html>

<html>



<body>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
        <h3 class="blue-grey-text text-darken-4">Liste des incidents</h3>
    </div>
  </div>


  <div class="container">
    <div class="section">
      <div class="responsive-table">
      
      <br></br>
      <a href="http://localhost:8888/gestion_incident/application-de-gestion-d-incidents/public/bug/add" class="btn-floating btn-large waves-effect waves-light blue"><i class="material-icons">add</i></a>
      <h6 class="text">Rapporter un incident</h6>


      <table>
        <thead>
          <tr>
              <th>id </th>
              <th>sujet</th>
              <th>date</th>
              <th>cloture</th>
              <th></th>
          </tr> 
        </thead>

        <tbody>
        <?php
              foreach ($bugs as $bug) {
                echo '            
                <tr>
                <td>'.$bug->getId().'</td>
                <td>'.$bug->getTitle().'</td>
                <td>'.$bug->getCreatedAt()->format("Y-m-d").'</td>
                <td></td>
                <td><a href="'.PUBLIC_PATH.'bug/show/'.$bug->getId().'">Afficher</a></td>
                </tr>';
              }
            ?>
        </tbody>
      </table>

    </div>

    </div>
    <br><br>
  </div>



</body>

</html>