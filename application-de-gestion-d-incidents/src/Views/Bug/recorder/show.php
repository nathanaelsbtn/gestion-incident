<?php

    /** @var $bug \BugApp\Models\Bug */

    $bug = $parameters['bug'];
    include ('../src/Views/inc/header.php');
    include ('../src/Views/inc/nav.php');

?>

<!DOCTYPE html>

<html>

<body>


  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="col l2 s6">
        <h3 class="blue-grey-text text-darken-4 ">Fiche descriptive d'incident</h3></div>
            <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">
      <div class="responsive-form">
        <div class="row">
            <div class="col s3">Nom de l'incident :</div>
            <div class="col s4"><?php  echo $bug->gettitle(); ?></div>
            <div class="col s3">Date d'observation : <?php  echo $bug->getCreatedAt()->format("d/m/Y");?></div>
            <div class="col s3"><br>Description de l'incident :</div>
            <div class="col s9"><br><?php  echo $bug->getdescription(); ?></div>
          </div>

    </div>
  </div>
</div>



        
    </div>

   

</body>

</html>