<?php

    /** @var $bug \BugApp\Models\Bug */

    $bugs = $parameters['bugs'];
    include ('../src/Views/inc/header.php');
    include ('../src/Views/inc/nav.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Gestion incident Ingenieur</title>

</head>
<body>
  
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="col l2 s2">
        <h3 class="blue-grey-text text-darken-4">Liste des incidents</h3></div>
      <div class="col l2 s2">
        <form action="#">
          <p>
            <label>
              <input type="checkbox" class="filled-in"  id="non-cloture" />
              <span>Afficher uniquement les incidents non-cloturés</span>
            </label>
          </p>
          <p>
            <label>
              <input type="checkbox" class="filled-in" id="affiche-assigner" />
              <span>Afficher uniquement les incidents que je me suis assignés</span>
            </label>
          </p>        
        </form>

    </div>
  </div>


  <div class="container">
    <div class="section">
      <div class="responsive-table">

      <table>
      <tbody>

<?php foreach ($bugs as $bug) { ?>
    <?php /** @var $bug \BugApp\Models\Bug */ ?>
    <tr id="bug_<?= $bug->getId(); ?>" class="bug">
        <td><?= $bug->getId(); ?></td>
        <td><?= $bug->getTitle(); ?></td>
        <td><?= $bug->getCreatedAt()->format('d/m/Y'); ?></td>
        <td><a href="<?= PUBLIC_PATH; ?>engineer/show/<?= $bug->getId(); ?>">Afficher</a></td>
        <td><a href="<?= PUBLIC_PATH; ?>engineer/update/<?= $bug->getId(); ?>&action=assign">Assigner</a></td>
        <td><a href="<?= PUBLIC_PATH; ?>engineer/show/<?= $bug->getId(); ?>&action=close">Clôturer</a></td>
        <td></td>
    </tr>

<?php } ?>

</tbody>
      </table>

    </div>

    </div>
  </div>

  </body>
</html>
