<?php

    /** @var $bug \BugApp\Models\Bug */


    $bug = $parameters['bug'];
    include ('../src/Views/inc/header.php');
    include ('../src/Views/inc/nav.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Fiche descriptive d'incident</title>

  
  
</head>
<body>
    
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
    <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-3" href="<?= PUBLIC_PATH ?>bug/engineer"><i class="material-icons">arrow_back</i></a>
      <br><br>
      <div class="col l2 s6">
        <h3 class="blue-grey-text text-darken-4 ">Fiche descriptive d'incident <a href="<?=PUBLIC_PATH?>bug/update/<?= $bug->getId() ?>"><i class="small material-icons">create</i></a></p></h3></div>
            <br><br>

    </div>
  </div>


  <div class="container">
  <div class="section">
    <div class="responsive-form">
    <div class="row">
        

  
<div class="container">
  <div class="section">
    <div class="responsive-form">
      <div class="row">
          <div class="col s3">Nom de l'incident :</div>
          <div class="col s4"><?php  echo $bug->gettitle(); ?></div>
          <div class="col s3">Date d'observation : <?php  echo $bug->getCreatedAt()->format("d/m/Y");?></div>
          <div class="col s3"><br>Description de l'incident :</div>
          <div class="col s9"><br><?php  echo $bug->getdescription(); ?></div>
        </div>

  </div>
</div>
</div>



      
  </div>

 

</body>

</html>
