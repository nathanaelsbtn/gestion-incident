<?php

    /** @var $bug \BugApp\Models\Bug */


    include ('../src/Views/inc/header.php');
    include ('../src/Views/inc/nav.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Fiche descriptive d'incdent</title>

</head>
<body>
    <nav class="blue-grey lighten-2" role="navigation">
      <a href="list_incident_client.html" class="waves-effect waves-light btn-large blue-grey  left"><h5>Retout a la liste</h5></a>
      <div class="nav-wrapper container"><i class="blue-text material-icons right">person</i>
    
      <ul class="right hide-on-med-and-down">
      </ul>
      <ul id="nav-mobile" class="sidenav">
      </ul>

    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <a class="btn-floating btn-large waves-effect waves-light blue-grey darken-3" href="<?= PUBLIC_PATH ?>bug/engineer"><i class="material-icons">arrow_back</i></a>
      <div class="col l2 s6">
        <h3 class="blue-grey-text text-darken-4 ">Modification du rapport d'incident <i class="blue-text material-icons">mode_edit</i></h3></div>
            <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">
      <div class="responsive-form">

        

        <div class="row">
            <div class="col s3">Nom de l'incident :</div>
            <div class="col s4">Vulputate commodo lectus</div>
            <div class="col s3">Date d'observation : 28/09/2020</div>
            <div class="col s3"><br>Description de l'incident :</div>
            <div class="col s9"><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit tincidunt id. Sed rhoncus, tortor sed eleifend tristique, tortor mauris molestie elit, et lacinia ipsum quam nec dui. Quisque nec mauris sit amet elit iaculis pretium sit amet quis magna. Aenean velit odio, elementum in tempus ut, vehicula eu diam. Pellentesque rhoncus aliquam mattis. Ut vulputate eros sed felis sodales nec vulputate justo hendrerit. Vivamus varius pretium ligula, a aliquam odio euismod sit amet. Quisque laoreet sem sit amet orci ullamcorper at ultricies metus viverra. Pellentesque arcu mauris, malesuada quis ornare accumsan, blandit sed diam.</div>
          </div>

          <a class="btn disabled right">Enregistrer</a>    
          <form action="cloturer">
            <p>
              <label>
                <input type="checkbox" />
                <span>Cloture de l'incident</span>
              </label>
            </p>
        
               
     
    </div>
  </div>
</div>
<footer class="page-footer">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Footer Content</h5>
          <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      © 2014 Copyright Text
      <a class="page-footer blue-grey" href="#!">More Links</a>
      </div>
    </div>
  </footer>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
